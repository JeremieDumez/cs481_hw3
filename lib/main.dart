import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hw3 - Changing TextStyle'),
        ),
        body: Center(
          child:
          MyStatefulWidget()
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  List<TextStyle> TextStyleList = [
    TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.black,),
    TextStyle(fontStyle: FontStyle.italic, fontSize: 10, color: Colors.green,),
    TextStyle(fontWeight: FontWeight.w900, fontSize: 28, color: Colors.red,),
    TextStyle(fontWeight: FontWeight.w600, fontSize: 35, color: Colors.amber),
    TextStyle(fontWeight: FontWeight.w200, fontSize: 20, color: Colors.blue,
        fontStyle: FontStyle.italic,),
  ];
  int index = 0;
  int check = 0;
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text('Please press the button to change my style',),
        IconButton(
          icon: Icon(Icons.refresh_outlined, size: 40),
          onPressed: () {
            setState(() {
              index += 1;
              if (index > 4)
                index = 0;
            });
          },
        ),
        Text(
          'Here is the text to change.',
          style: TextStyleList[index],
          softWrap: true,
        )
      ],
    );
  }
}